import {
	faAngleLeft,
	faAngleRight,
	faHome,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useRouter } from "next/dist/client/router";
import { FC, ReactNode, useEffect } from "react";

export interface SlideControlNavAction {
	forward?: boolean;
	backward?: boolean;
}

interface SlideControlProps {
	children: ReactNode;
	ableToGo?: SlideControlNavAction;
}

export const SlideControls: FC<SlideControlProps> = ({
	children,
	ableToGo,
}) => {
	const router = useRouter();
	const iconBaseClass = "w-10 mr-3 text-gray-800 hover:text-gray-400";

	const move = (action: "home" | "right" | "left") => {
		const current = +(router.pathname.split("/")[1] || 0);

		if (action === "home") return router.push("/");

		console.log(current);

		if (action == "right") {
			router.push(`${current + 1}`);
			return;
		}

		if (current - 1 < 0) return;
		router.push(current - 1 === 0 ? "/" : `/${current - 1}`);
	};

	useEffect(() => {
		const evt = e => {
			if (e.key === "ArrowRight" && ableToGo?.forward) move("right");
			if (e.key === "ArrowLeft" && ableToGo?.backward) move("left");
		};
		window.addEventListener("keydown", evt);
		return () => window.removeEventListener("keydown", evt);
	});

	return (
		<article>
			{children}
			<nav className="fixed bottom-2 right-2 cursor-pointer flex">
				{ableToGo?.backward && (
					<FontAwesomeIcon
						icon={faAngleLeft}
						className={iconBaseClass}
						onClick={() => move("left")}
					></FontAwesomeIcon>
				)}
				<FontAwesomeIcon
					icon={faHome}
					className={iconBaseClass}
					onClick={() => move("home")}
				></FontAwesomeIcon>
				{ableToGo?.forward && (
					<FontAwesomeIcon
						icon={faAngleRight}
						className={iconBaseClass}
						onClick={() => move("right")}
					></FontAwesomeIcon>
				)}
			</nav>
		</article>
	);
};
