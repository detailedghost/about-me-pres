import Head from "next/dist/shared/lib/head";
import { FC, ReactNode } from "react";

export const Layout: FC<{
	title?: string;
	children?: ReactNode;
}> = ({ title, children }) => (
	<>
		<Head>
			<title>{title || "Dante Burgos Presentation"}</title>
			<meta name="description" content="A simple presentation application" />
			<link rel="icon" href="/favicon.ico" />
		</Head>
		<main className="w-screen h-screen grid place-items-center bg-gray-600 text-indigo-200">
			{children}
		</main>
	</>
);
