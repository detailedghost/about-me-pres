import { FC, ReactNode } from "react";
import { SlideControlNavAction, SlideControls } from "./SlideControls";
// import styles from "../styles/Slide.module.css";
import styles from "../styles/Slide.module.css";

interface SlideProps {
	title: string;
	navConfig?: SlideControlNavAction;
	children?: ReactNode;
}

export const Slide: FC<SlideProps> = ({ title, children, navConfig }) => (
	<SlideControls ableToGo={navConfig}>
		<article>
			<header>
				<h1 className={styles.slideTitle}>{title}</h1>
			</header>
			<section className={styles.slideBody}>{children}</section>
		</article>
	</SlideControls>
);
