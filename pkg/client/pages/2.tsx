import type { NextPage } from "next";
import { Layout } from "../components/Layout";
import { Slide } from "../components/Slide";

const Home: NextPage = () => {
	const skills = [
		"C# .Net",
		"(T)SQL",
		"Postgresdb",
		"JS / TS",
		"React",
		"Angular 11",
		"NodeJs / Express",
		"BASH",
		"Gitlab Devops",
		"AWS Services",
	];
	return (
		<Layout>
			<Slide title="Skills Slide" navConfig={{ backward: true, forward: true }}>
				<ul>
					{skills.map((s, i) => (
						<li key={i}>{s}</li>
					))}
					<li></li>
				</ul>
			</Slide>
		</Layout>
	);
};

export default Home;
