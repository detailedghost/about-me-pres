import { faLink } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import type { NextPage } from "next";
import { Layout } from "../components/Layout";
import { Slide } from "../components/Slide";

const Home: NextPage = () => {
	return (
		<Layout>
			<Slide
				title="Example Project"
				navConfig={{ backward: true, forward: true }}
			>
				<a
					className="text-indigo-400 hover:text-yellow-400 text-center"
					href="https://demo-gist-viewer.vercel.app/"
				>
					<FontAwesomeIcon
						className="inline mr-2"
						width="32"
						icon={faLink}
						target="_blank"
					></FontAwesomeIcon>
					Gist Viewer
				</a>
			</Slide>
		</Layout>
	);
};

export default Home;
