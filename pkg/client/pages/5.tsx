import type { NextPage } from "next";
import { Layout } from "../components/Layout";
import { Slide } from "../components/Slide";

const Home: NextPage = () => {
	return (
		<Layout>
			<Slide title="" navConfig={{ backward: true, forward: false }}>
				<h2>Thank You</h2>
			</Slide>
		</Layout>
	);
};

export default Home;
