import type { NextPage } from "next";
import { Layout } from "../components/Layout";
import { Slide } from "../components/Slide";

const Home: NextPage = () => {
	const timeline = [
		{
			job: "HCA",
			time: "May 2014 - May 2017",
			title: "Junior > Mid App. Developer",
			apps: ["Chronic Disease Registry, Nurse Shift Bid, Hospital Downtime"],
		},
		{
			job: "Psych Now",
			time: "Aug. 2017 - Oct. 2019",
			title: "Lead App. Developer",
			apps: ["Psych Now Core EHR"],
		},
		{
			job: "Cognizant Softvision",
			time: "Sept. 2019 -",
			title: "Senior > Lead App. Developer",
			apps: ["NCR - Digital Signage Canvas", "Aventiv Securus - Allpaid"],
		},
	];
	return (
		<Layout>
			<Slide title="A quick bio" navConfig={{ backward: true, forward: true }}>
				<h2>Professional timeline</h2>
				<section className="flex mb-2">
					{timeline.map((item, i) => (
						<div
							key={i}
							className="container border-r-2 first:border-l-2 m-2 p-4 border-radius-md"
						>
							<h3 className="text-2xl">{item.job}</h3>
							<h4 className="text-xl">{item.title}</h4>
							<h5 className="text-base">{item.time}</h5>
							<ul className="list-unstyled">
								{item.apps.map((app, ai) => (
									<li className="text-sm block" key={`${app}-${ai}`}>
										{app}
									</li>
								))}
							</ul>
						</div>
					))}
				</section>
			</Slide>
		</Layout>
	);
};

export default Home;
