import type { NextPage } from "next";
import { Layout } from "../components/Layout";
import { Slide } from "../components/Slide";
import Image from "next/image";

const Home: NextPage = () => {
	return (
		<Layout>
			<Slide title="" navConfig={{ backward: true, forward: true }}>
				<section className="flex">
					<section className="mr-2">
						<section>
							<h2>A Husband</h2>
							<Image
								width={500}
								height={250}
								src="/leila.jpg"
								alt="My bride to be"
							/>
						</section>
						<section>
							<h2>Off Time Baker</h2>
							<div className="flex">
								<div className="mr-1">
									<Image
										src="/pretzel.jpg"
										width="250"
										height="250"
										alt="Pretzel"
										className="mr-1"
									></Image>
								</div>
								<div>
									<Image
										className="mr-1"
										src="/crinkles.jpg"
										width="150"
										height="250"
										alt="Crinkles"
									></Image>
								</div>
							</div>
							<div className="mr-1">
								<Image
									src="/bun.jpg"
									width="500"
									height="200"
									alt="Tiger Rolls"
									className="mr-1"
								></Image>
							</div>
						</section>
					</section>
					<section className="mt-32 ml-4">
						<h2>A Cat Dad</h2>
						<div className="flex">
							<div className="mr-2">
								<Image
									width={(1 / 3) * 1000}
									height={(1 / 2) * 1000}
									src="/buddy.jpg"
									alt="Buddy the cat"
								/>
							</div>
							<div>
								<Image
									width={250}
									height={500}
									src="/isis.jpg"
									alt="Isis the cat"
								/>
							</div>
						</div>
					</section>
				</section>
			</Slide>
		</Layout>
	);
};

export default Home;
