import type { NextPage } from "next";
import { Layout } from "../components/Layout";
import { Slide } from "../components/Slide";

const Home: NextPage = () => {
	return (
		<Layout>
			<Slide title="A Personal Introduction 👋" navConfig={{ forward: true }}>
				<section>
					<h1 className="text-center">Dante Burgos</h1>
				</section>
			</Slide>
		</Layout>
	);
};

export default Home;
