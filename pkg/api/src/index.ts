import Express, { Application, RequestHandler } from "express";
import Dotenv from "dotenv";
import { isDev } from "./util";

//* Routes
import UserRoute from "./routes/user";

const dotenv = Dotenv.config({
	path: isDev ? "../../.env.local" : "../../.env",
});

const app: Application = Express();
const port: number | string = process.env.API_PORT || 3000;

app.get("/", (req, res, next) => {
	res.send({
		message: "Welcome to my basic presentation API. Not much to see here. ",
	});
});

app.get("/health", (req, res, next) => {
	//? Usually add major healthcheck, but speed running this right now
	res.send("good");
});

app.use("/user", UserRoute);

try {
	if (dotenv.error) throw "Issue loading .env";

	app.listen(port, () => {
		console.log(`Server launched at port ${port}`);
		console.log(`Using Environement ${process.env.ENVIRONMENT}`);
	});
} catch (error) {
	console.error("Problem on the server: " + error);
	process.exit(1);
}
