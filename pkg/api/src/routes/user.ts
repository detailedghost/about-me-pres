import { Router } from "express";
import { DbConnection } from "../db";

const router = Router();

router.get("/", async (req, res) => {
	try {
		const person = await DbConnection.create()("person").first();
		res.send(person);
	} catch (error) {
		console.log(error);
		res.sendStatus(500);
	}
});

export default router;
