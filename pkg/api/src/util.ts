import { env } from "process";

export const isDev = env.ENVIRONMENT !== "production";
