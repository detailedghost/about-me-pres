import Knex from "knex";
import path from "path";
import { isDev } from "./util";

//* Keeping things simple, could check out adding stuff like migrations, seeding, but kept simple for now
export class DbConnection {
	static create() {
		return Knex({
			client: "sqlite",
			connection: {
				filename: path.join(__dirname, "..", "db.sqlite3"),
			},
			debug: isDev,
		});
	}
}
